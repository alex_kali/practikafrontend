import React, {memo} from 'react';
import {
  BrowserRouter as Router,
  useRoutes
} from "react-router-dom";
import './App.css';
import 'rc-slider/assets/index.css';
import {PageController} from "./pages/pageController";

function App() {
  const AppRoutes = memo(() => {
    return useRoutes([
      { path: "/:position/:page", element: <PageController /> },
    ]);
  });
  return (
    <Router>
      <AppRoutes />
    </Router>
  );
}

export default App;
