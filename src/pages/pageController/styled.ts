import styled from "styled-components";

export const PageControllerStyled = styled.div`
  width: 100%;
  height: 100%;
  margin-left: 0;
  transition-duration: 0.5s;
  &.topSliderRight{
    #topPage{
      margin-left: 100%;
      transition-duration: 0s;
    }
    #topAnimation{
      left: 0 !important;
      transition-duration: 0s;
      opacity: 1;
    }
  }
  &.topSliderLeft{
    #topPage{
      margin-left: -100%;
      transition-duration: 0s;
    }
    #topAnimation{
      left: 0 !important;
      transition-duration: 0s;
      opacity: 1;
    }
  }
  &.topToCenter{
    #topAnimation{
      left: 0 !important;
      top: 0 !important;
      transition-duration: 0s;
      opacity: 1;
    }
  }
  
  &.bottomSliderRight{
    #bottomPage{
      margin-left: 100%;
      transition-duration: 0s;
    }
    #bottomAnimation{
      left: 0 !important;
      transition-duration: 0s;
      opacity: 1;
    }
  }
  &.bottomSliderLeft{
    #bottomPage{
      margin-left: -100%;
      transition-duration: 0s;
    }
    #bottomAnimation{
      left: 0 !important;
      transition-duration: 0s;
      opacity: 1;
    }
  }
  &.bottomToCenter{
    #bottomAnimation{
      left: 0 !important;
      top: 0 !important;
      transition-duration: 0s;
      opacity: 1;
    }
  }
  &.centerToTop{
    #topPage{
      margin-top: -50%;
      transition-duration: 0s;
      opacity: 0;
    }
  }
  
  &.rightToCenter + div{
    #AnimationRightPageStyled{
      transition-duration: 0s !important;
      left: 0% !important;
    }
  }
  
  &.toLeft{
    margin-left: -100%;
  }
  
  & + div{
    left: 100%;
  }
  
  &.toLeft + div{
    left: 0%;
  }
`

export const AnimationTopStyled = styled.div`
  position: absolute;
  width: 100vw;
  //height: 100%;
  left: -100%;
  top: 0%;
  transition-duration: 0.5s;
  height: calc(100vh - 78px);
  opacity: 0;
`

export const AnimationBottomStyled = styled.div`
  position: absolute;
  //height: 100%;
  left: -100%;
  top: 0%;
  transition-duration: 0.5s;
  height: calc(100vh - 78px);
  width: 100vw;
  opacity: 0;
  overflow: hidden;
`



export const CenterPageStyled = styled.div<{position: string}>`
  height: calc(100% - 136px);
  transition-duration: 0.5s;
  overflow: hidden;
  ${props => props.position === 'top' ? `
    opacity: 0;
  ` : ``}
  
  ${props => props.position === 'bottom' ? 'height: 60px;' : ''}
`

export const WrapperTopPageStyled = styled.div<{position: string}>`
  transition-duration: 0.5s;
  height: 0px;
  overflow: hidden;
  position: relative;
  ${props => props.position === 'top' ? `
    height: calc(100% - 78px);
  ` : ``}
`

export const TopPageStyled = styled.div`
  transition-duration: 0.5s;
  height: calc(100vh - 78px);
  width: 100vw;
`

export const WrapperBottomPageStyled = styled.div`
  height: calc(100% - 156px);
  position: relative;
  overflow: hidden;
`

export const BottomPageStyled = styled.div`
  height: 100%;
  width: 100vw;
  transition-duration: 0.5s;
`

export const WrapperRightPageStyled = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  transition-duration: 0.5s;
  left: 0;
  top: 0;
`

export const RightPageStyled = styled.div`
  float: right;
  height: 100%;
  width: calc(100% - 162px);
`

export const AnimationRightPageStyled = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  left: 100%;
  top: 0%;
  transition-duration: 0.5s;
`