import React, {FC, useEffect, useRef} from "react";
import {Header} from "../../components/header";
import {HomePage} from "../home";
import {useParams} from "react-router-dom";
import {
  AnimationBottomStyled, AnimationRightPageStyled,
  AnimationTopStyled, BottomPageStyled,
  CenterPageStyled,
  PageControllerStyled, RightPageStyled,
  TopPageStyled, WrapperBottomPageStyled, WrapperRightPageStyled,
  WrapperTopPageStyled
} from "./styled";
import {PagesTop} from "../pagesTop";
import {PagesBottom} from "../bottomPages";
import {AuthMenu} from "../../components/authMenu";
import {RightPages} from "../rightPages";



export const PageController:FC = () => {
  const {position, page}:any = useParams()
  const refPosition = useRef()
  const refPage = useRef()


  if(refPage.current && refPosition.current && (refPage.current !== page)){
    let typeAnimation = '';
    if((refPosition.current === 'center') && (position === 'top')){
      typeAnimation = 'centerToTop'
    }
    if((refPosition.current !== position) && (refPosition.current === 'center')){
      console.info('default animation')
    }else if((refPosition.current !== position) && (refPosition.current !== 'center')){
      if(refPosition.current === 'top'){
        const clone = (document.getElementById('topPage') as HTMLElement).querySelectorAll('div')[0].cloneNode( true );
        (document.getElementById('topAnimation') as any).innerHTML = '';
        (document.getElementById('topAnimation') as any).appendChild(clone);
        (document.getElementById('topAnimation') as any).style.left = '0%';
        (document.getElementById('topAnimation') as any).style.top = '-100vw';
        typeAnimation = 'topToCenter'
      }else if(refPosition.current === 'bottom'){
        const clone = (document.getElementById('bottomPage') as HTMLElement).querySelectorAll('div')[0].cloneNode( true );
        (document.getElementById('bottomAnimation') as any).innerHTML = '';
        (document.getElementById('bottomAnimation') as any).appendChild(clone);
        (document.getElementById('bottomAnimation') as any).style.left = '0%';
        (document.getElementById('bottomAnimation') as any).style.top = '100%';
        typeAnimation = 'bottomToCenter'
      }

    }else if((refPosition.current === position) && (refPosition.current !== 'center')){
      if(refPosition.current === 'top'){
        const clone = (document.getElementById('topPage') as HTMLElement).querySelectorAll('div')[0].cloneNode( true );
        (document.getElementById('topAnimation') as any).innerHTML = '';
        (document.getElementById('topAnimation') as any).appendChild(clone);
        if(PagesTop[page]?.position > PagesTop[refPage.current]?.position){
          (document.getElementById('topAnimation') as any).style.left = '-100%';
          (document.getElementById('topAnimation') as any).style.top = '0%';
          typeAnimation = 'topSliderRight'
        }else{
          (document.getElementById('topAnimation') as any).style.left = '100%';
          (document.getElementById('topAnimation') as any).style.top = '0%';
          typeAnimation = 'topSliderLeft'
        }
      }else if(refPosition.current === 'bottom'){
        const clone = (document.getElementById('bottomPage') as HTMLElement).querySelectorAll('div')[0].cloneNode( true );
        (document.getElementById('bottomAnimation') as any).innerHTML = '';
        (document.getElementById('bottomAnimation') as any).appendChild(clone);
        if(PagesBottom[page]?.position > PagesBottom[refPage.current]?.position){

          (document.getElementById('bottomAnimation') as any).style.left = '-100%';
          (document.getElementById('bottomAnimation') as any).style.top = '0%';

          typeAnimation = 'bottomSliderRight'
        }else{
          (document.getElementById('bottomAnimation') as any).style.left = '100%';
          (document.getElementById('bottomAnimation') as any).style.top = '0%';

          typeAnimation = 'bottomSliderLeft'
        }
      }
    }

    if(position !== 'right' && refPosition.current === 'right'){
      const clone = (document.getElementById('rightPage') as HTMLElement).querySelectorAll('div')[0].cloneNode( true );
      (document.getElementById('AnimationRightPageStyled') as any).innerHTML = '';
      (document.getElementById('AnimationRightPageStyled') as any).appendChild(clone);

      (document.getElementById('pageController') as any).classList.remove('toLeft');
      typeAnimation = 'rightToCenter'
    }

    if(position === 'right' && refPosition.current !== 'right'){
      (document.getElementById('pageController') as any).classList.add('toLeft');
    }



    if(typeAnimation){
      (document.getElementById('pageController') as any).classList.add(typeAnimation);
      setTimeout(()=> {
        (document.getElementById('pageController') as any).classList.remove(typeAnimation)
      },10)
    }
  }

  refPosition.current = position
  refPage.current = page
  return (
    <>
      <PageControllerStyled id={'pageController'} className={position === 'right' ? 'toLeft' : ''}>
          <WrapperTopPageStyled position={position as string}>
            <TopPageStyled id={'topPage'}>
              {position === 'top' && PagesTop[page]?.component}
            </TopPageStyled>
            <AnimationTopStyled id={'topAnimation'}>

            </AnimationTopStyled>
          </WrapperTopPageStyled>


        <Header />
        <CenterPageStyled position={position as string}>
          <HomePage />
        </CenterPageStyled>
        <WrapperBottomPageStyled>
          <BottomPageStyled id={'bottomPage'}>
            {position === 'bottom' && PagesBottom[page]?.component}
          </BottomPageStyled>
          <AnimationBottomStyled id={'bottomAnimation'}>

          </AnimationBottomStyled>
        </WrapperBottomPageStyled>
      </PageControllerStyled>
      <WrapperRightPageStyled>
        <div style={{width: '100%', height: '100%'}} id={'rightPage'}>
          {position === 'right' &&
            <div style={{width: '100%', height: '100%'}}>
              <AuthMenu/>
              <RightPageStyled>
                {RightPages[page]?.component}
              </RightPageStyled>
            </div>
          }
        </div>
        <AnimationRightPageStyled id={'AnimationRightPageStyled'}/>
      </WrapperRightPageStyled>
    </>
  )
}