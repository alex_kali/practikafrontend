import {FC} from "react";
import {
  AddStyled,
  CenterDotsStyled,
  MenuItemNameStyled,
  MenuItemPriceStyled,
  MenuItemStyled,
  MoreStyled
} from "./styled";

export interface IItemMenu {
  id: number | string;
  name: string;
  price: string;
  image: string;
  ingredients: Array<string>;
  proteins: string;
  fats: string;
  carbohydrates: string;
  calories: string;
}

export const MenuItemView:FC<{item: IItemMenu, setItemView: any, isActive: boolean}> = (props) => {
  return(
    <MenuItemStyled isActive={props.isActive}>
      <MenuItemNameStyled>{props.item.name}</MenuItemNameStyled>
      <CenterDotsStyled />
      <MenuItemPriceStyled>{props.item.price}</MenuItemPriceStyled>
      <AddStyled/>
      <MoreStyled onClick={() =>{if(!props.isActive)(props.setItemView(props.item))}}/>
    </MenuItemStyled>
  )
}