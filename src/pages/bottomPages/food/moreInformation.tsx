import {
  CenterDotsStyled,
  ImageStyled,
  MenuItemNameStyled,
  MenuItemPriceStyled,
  MoreInformationStyled,
  MenuItemStyled,
  MoreInformationItem, MoreInformationTitleStyled, ArrowStyled, IngredientsItemStyled
} from "./styled";
import {FC} from "react";
import {IItemMenu} from "./menuItemView";
import {MoreInformationTextStyled} from "../styled";

export const MoreInformation:FC<{item: IItemMenu | undefined, setIsView: any, isView: boolean}> = (props) => {
  console.log(props.item?.image)
  return (
    <MoreInformationStyled isView={props.isView} id={'MoreInformationStyled'}>
      {props.item &&
        <div style={{height: '100%', width: '100%'}}>
          <MoreInformationTitleStyled>{props.item.name}</MoreInformationTitleStyled>
          <ImageStyled style={{backgroundImage:  `url(${props.item.image})`}}/>
          <MoreInformationItem>
            <MenuItemNameStyled>Белки</MenuItemNameStyled>
            <CenterDotsStyled />
            <MenuItemPriceStyled>{props.item.proteins}</MenuItemPriceStyled>
          </MoreInformationItem>
          <MoreInformationItem>
            <MenuItemNameStyled>Жиры</MenuItemNameStyled>
            <CenterDotsStyled />
            <MenuItemPriceStyled>{props.item.fats}</MenuItemPriceStyled>
          </MoreInformationItem>
          <MoreInformationItem>
            <MenuItemNameStyled>Углеводы</MenuItemNameStyled>
            <CenterDotsStyled />
            <MenuItemPriceStyled>{props.item.carbohydrates}</MenuItemPriceStyled>
          </MoreInformationItem>
          <MoreInformationItem>
            <MenuItemNameStyled>Калории</MenuItemNameStyled>
            <CenterDotsStyled />
            <MenuItemPriceStyled>{props.item.calories}</MenuItemPriceStyled>
          </MoreInformationItem>
          <MoreInformationTextStyled>{props.item.ingredients.map((item)=><IngredientsItemStyled>{item}</IngredientsItemStyled>)}</MoreInformationTextStyled>
          <ArrowStyled onClick={() => props.setIsView()}/>
        </div>
      }
    </MoreInformationStyled>
  )
}