import {FC, useEffect, useState} from "react";
import {
  FoodPageStyled,
  MenuFoodStyled,
  MoreInformationAnimationStyled,
  TitleStyled,
  WrapperFoodPageStyled
} from "./styled";
import {foodData} from "../../../static/food";
import {IItemMenu, MenuItemView} from "./menuItemView";
import {MoreInformation} from "./moreInformation";

const names:any = {
  meatDishes: 'Мясные блюда',
  coldDishes: 'Холодные блюда',
  desserts: 'Десерты',
  alcoholicDrinks: 'Алкогольные напитки',
  hotDrinks: 'Горячие напитки',
  coldDrinks: 'Холодные напитки',
}

export const FoodPage: FC<{name: string}> = (props) => {
  const [isView, setIsView] = useState(false)
  const [item, setItem] = useState<IItemMenu | undefined>()

  useEffect(()=>{
    setIsView(false)
    setItem(undefined)
  }, [props.name])

  const setViewItem = (newItem:IItemMenu | undefined) => {
    let typeAnimation = ''
    if(item){
      const clone = (document.getElementById('MoreInformationStyled') as HTMLElement).querySelectorAll('div')[0].cloneNode( true );
      (document.getElementById('MoreInformationAnimationStyled') as any).innerHTML = '';
      (document.getElementById('MoreInformationAnimationStyled') as any).appendChild(clone);
    }
    if(item && !newItem){
      (document.getElementById('MoreInformationAnimationStyled') as any).style.left = '-100%';
      (document.getElementById('MoreInformationAnimationStyled') as any).style.top = '30px';
      typeAnimation = 'sliderToCenter'
    }

    if(newItem){
      if(item){
        if(newItem.id < item.id){
          (document.getElementById('MoreInformationAnimationStyled') as any).style.left = '5%';
          (document.getElementById('MoreInformationAnimationStyled') as any).style.top = '100%';
          typeAnimation = 'sliderToTop'
        }else{
          (document.getElementById('MoreInformationAnimationStyled') as any).style.left = '5%';
          (document.getElementById('MoreInformationAnimationStyled') as any).style.top = '-100%';
          typeAnimation = 'sliderToBottom'
        }
      }
      setIsView(true)
      setItem(newItem)
    }else{
      setIsView(false)
      setItem(undefined)
    }
    if(typeAnimation){
      (document.getElementById('WrapperFoodPageStyled') as any).classList.add(typeAnimation);
      setTimeout(()=> {
        (document.getElementById('WrapperFoodPageStyled') as any).classList.remove(typeAnimation)
      },10)
    }
  }
  return (
    <WrapperFoodPageStyled id={'WrapperFoodPageStyled'}>
      <MoreInformation isView={isView} item={item} setIsView={() => setViewItem(undefined)}/>
      <MoreInformationAnimationStyled id={'MoreInformationAnimationStyled'}/>
      <FoodPageStyled isView={isView}>
        <TitleStyled>{names[props.name]}</TitleStyled>
        <MenuFoodStyled>
          {foodData[props.name].map((value:IItemMenu)=><MenuItemView item={value} setItemView={setViewItem} isActive={item?.id == value.id}/>)}
        </MenuFoodStyled>
      </FoodPageStyled>
    </WrapperFoodPageStyled>
  )
}