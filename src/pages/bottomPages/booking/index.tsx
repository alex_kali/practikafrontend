import {FC} from "react";
import {BookingStyled, ButtonStyled, TitleStyled} from "./styled";
import {Place} from "./place";
import {Time} from "./time";

export const Booking:FC = () => {
  return (
    <BookingStyled>
      <TitleStyled>Рассадка</TitleStyled>
      <Place />
      <TitleStyled>Время брони</TitleStyled>
      <Time />
      <ButtonStyled>Забронировать</ButtonStyled>
    </BookingStyled>
  )
}