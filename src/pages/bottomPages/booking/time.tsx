import {FC} from "react";
import {TimeItemStyled, TimeStyled} from "./styled";

export const Time:FC = () => {
  const timeItem = []
  for(let i = 0; i < 5; i++){
    timeItem.push(i)
  }
  const today = new Date();
  today.setDate(today.getDate()-1);
  let days = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];

  const viewDay = () => {
    today.setDate(today.getDate()+1);
    return <><div style={{marginBottom: 6}}>{days[today.getDay()]}</div><div>{today.toLocaleString('ru', {month: 'long', day: 'numeric'})}</div></>
  }

  return (
    <TimeStyled>
      {timeItem.map((item) => (
        <TimeItemStyled>
          {viewDay()}
        </TimeItemStyled>
      ))}
    </TimeStyled>
  )
}