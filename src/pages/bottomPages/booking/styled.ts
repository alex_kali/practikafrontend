import styled from "styled-components";

export const BookingStyled = styled.div`
  width: 700px;
  margin-top: 30px;
  height: calc(100% - 40px);
  margin-left: auto;
  margin-right: auto;
`

export const PlaceStyled = styled.div`
  width: 696px;
  height: 225px;
  border: 2px solid #73777D;
  position: relative;
  margin-top: 10px;
`

export const PlaceItemStyled = styled.div`
  position: absolute;
  left: 16px;
  top: 10px;
  border: 2px solid #73777D;
  width: 20px;
  height: 20px;
  cursor: pointer;

  display: flex;
  justify-content: center;
  color: #73777D;
  font-size: 12px;
  line-height: 20px;
  &:before{
    content: '';
    display: block;
    position: absolute;
    left: -11px;
    top: -1px;
    border-left: 2px solid #73777D;
    border-bottom: 2px solid #73777D;
    width: 4px;
    height: 8px;
  }
  &:after{
    content: '';
    display: block;
    position: absolute;
    left: -11px;
    top: 12px;
    border-left: 2px solid #73777D;
    border-bottom: 2px solid #73777D;
    width: 4px;
    height: 8px;
  }
  
  &:hover{
    border-color: #FF9100;
    color: #FF9100;
    &:after , &:before, & > div:after,& > div:before{
      border-color: #FF9100;
    }
  }
`

export const Dec1PlaceItemStyled = styled.div`
  &:before{
    content: '';
    display: block;
    position: absolute;
    right: -11px;
    top: -1px;
    border-right: 2px solid #73777D;
    border-bottom: 2px solid #73777D;
    width: 4px;
    height: 8px;
  }
  &:after{
    content: '';
    display: block;
    position: absolute;
    right: -11px;
    top: 12px;
    border-right: 2px solid #73777D;
    border-bottom: 2px solid #73777D;
    width: 4px;
    height: 8px;
  }
`

export const TimeStyled = styled.div`
  margin-top: 10px;
  width: 100%;
`

export const TimeItemStyled = styled.div`
  width: calc(20% - 2px);
  height: 60px;
  float: left;
  border: 2px solid #73777D;
  margin-left: -2px;
  cursor: pointer;
  color: #73777D;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  &:first-child{
    margin-left: 0;
    width: calc(20% - 4px);
  }
  &:hover{
    border-color: #FF9100;
    color: #FF9100;
    z-index: 2;
    position: relative;
  }
`

export const TitleStyled = styled.div`
  margin-top: 20px;
  width: 100%;
  text-align: center;
  font-size: 22px;
  color: #73777D;
`

export const ButtonStyled = styled.div`
  margin-top: 30px;
  float: right;
  font-size: 20px;
  border: 2px solid #73777D;
  padding: 8px;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 10px;
  border-radius: 20px;
  color: #73777D;
  cursor: pointer;
  &:hover{
    border-color: #FF9100;
    color: #FF9100;
  }
`