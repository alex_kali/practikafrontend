import {FC} from "react";
import {Dec1PlaceItemStyled, PlaceItemStyled, PlaceStyled} from "./styled";

export const Place:FC = () => {
  return (
    <PlaceStyled>
      <PlaceItemStyled style={{left: 16}}>1<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{left: 76}}>2<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{left: 136}}>3<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{left: 196}}>4<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{left: 256}}>5<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{left: 416}}>6<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{left: 476}}>7<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{left: 536}}>8<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{left: 596}}>9<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{left: 656}}>10<Dec1PlaceItemStyled/></PlaceItemStyled>

      <PlaceItemStyled style={{top: 70,left: 16}}>11<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 70,left: 76}}>12<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 70,left: 136}}>13<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 70,left: 196}}>14<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 70,left: 256}}>15<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 70,left: 416}}>16<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 70,left: 476}}>17<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 70,left: 536}}>18<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 70,left: 596}}>19<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 70,left: 656}}>20<Dec1PlaceItemStyled/></PlaceItemStyled>

      <PlaceItemStyled style={{top: 130,left: 16}}>21<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 130,left: 76}}>22<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 130,left: 136}}>23<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 130,left: 196}}>24<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 130,left: 256}}>25<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 130,left: 416}}>26<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 130,left: 476}}>27<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 130,left: 536}}>28<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 130,left: 596}}>29<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 130,left: 656}}>30<Dec1PlaceItemStyled/></PlaceItemStyled>

      <PlaceItemStyled style={{top: 190,left: 16}}>31<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 190,left: 76}}>32<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 190,left: 136}}>33<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 190,left: 196}}>34<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 190,left: 256}}>35<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 190,left: 416}}>36<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 190,left: 476}}>37<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 190,left: 536}}>38<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 190,left: 596}}>39<Dec1PlaceItemStyled/></PlaceItemStyled>
      <PlaceItemStyled style={{top: 190,left: 656}}>40<Dec1PlaceItemStyled/></PlaceItemStyled>
    </PlaceStyled>
  )
}