import styled from "styled-components";

export const DeliveryStyled = styled.div`
  width: 100%;
  height: 100%;
`

export const TitleStyled = styled.div`
  color: #73777D;
  font-family: "LatoWebBold", sans-serif;
  font-size: 32px;
  width: 80%;
  height: 100%;
  margin-left: 10%;
  text-align: center;
  
  display: flex;
  align-items: center;
  justify-content: center;
`