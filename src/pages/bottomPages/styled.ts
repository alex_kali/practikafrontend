import styled from "styled-components";

export const LeftBlockStyled = styled.div`
  height: 100%;
  width: 500px;
  float: left;
  margin-top: 130px;
`

export const RightBlockStyled = styled.div`
  height: 100%;
  width: calc(100% - 500px);
  float: left;
  overflow-y: auto;
  & .gmnoprint , button{
    display: none;
  }
`

export const TitleStyled = styled.div`
  color: white;
  font-family: "LatoWebBold", sans-serif;
  font-size: 32px;
  margin-bottom: 60px;
  text-align: center;
`

export const TextStyled = styled.div`
  margin-bottom: 30px;
  font-size: 20px;
  color: #7A7F84;
  &:first-letter{
    color: white;
  }
`

export const MoreInformationTextStyled = styled.div`
  margin-top: 50px;
  height: 100px;
  color: #73777D;
  max-width: 640px;
  width: calc(100% - 80px);
  margin-left: auto;
  margin-right: auto;
`
