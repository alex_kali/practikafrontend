import {FC} from "react";
import png1 from "./icons/1.jpg";
import {EventStyled, ImageStyled, MoreStyled, TextStyled, TitleStyled} from "./styled";

export interface IEvent {
  name: string;
  image: string;
  text: string;
  price: string;
  countPeople: string;
}

export const Event:FC<{event: IEvent}> = (props) => {
  return (
    <EventStyled>
      <ImageStyled style={{backgroundImage: `url(${props.event.image}`}}/>
      <TitleStyled>{props.event.name}</TitleStyled>
      <TextStyled>{props.event.text}</TextStyled>
      <MoreStyled/>
    </EventStyled>
  )
}