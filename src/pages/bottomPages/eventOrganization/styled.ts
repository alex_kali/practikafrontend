import styled from "styled-components";

export const EventOrganisationStyled = styled.div`
  width: calc(100% - 80px);
  height: calc(100% - 30px);
  padding-top: 20px;
  padding-bottom: 10px;
  padding-left: 40px;
  padding-right: 40px;
`

export const EventStyled = styled.div`
  height: 300px;
  width: 93%;
  margin-left: 5%;
  margin-top: 20px;
  position: relative;
`

export const ImageStyled = styled.div`
  height: 100%;
  width: 40%;
  background-size: cover;
  background-position: center;
  float: left;
  border-radius: 8px;
`

export const TextStyled = styled.div`
  float: right;
  height: 66%;
  width: 50%;
  margin-right: 5%;
  margin-top: 2%;
  color: #7A7F84;
  font-size: 19px;
  overflow: hidden;
  text-overflow: ellipsis;
`

export const TitleStyled = styled.div`
  width: 60%;
  margin-left: 40%;
  text-align: center;
  font-size: 20px;
  color: #FF9100;
`

export const MoreStyled = styled.div`
  position: absolute;
  right: 20px;
  bottom: 50px;
  cursor: pointer;
  color: #FF9100;
  font-family: "LatoWebLight",sans-serif;
`