import png1 from "./icons/1.jpg"

export const eventOrganisationsExample = [
  {
    name: 'Корпоратив',
    image: png1,
    text: 'Традиция проведения организованного совместного отдыха сотрудников из одной компании уже прочно вошла в корпоративную культуру в России – с чем согласны все. Однако далеко не все в полной мере понимают и осознают, зачем нужны эти мероприятия. Ведь любой корпоратив – это прежде всего мощный инструмент HR-политики в бизнесе.',
    price: '10 000р.',
    countPeople: '30 чел.',
  },
  {
    name: 'Корпоратив',
    image: png1,
    text: 'Традиция проведения организованного совместного отдыха сотрудников из одной компании уже прочно вошла в корпоративную культуру в России – с чем согласны все. Однако далеко не все в полной мере понимают и осознают, зачем нужны эти мероприятия. Ведь любой корпоратив – это прежде всего мощный инструмент HR-политики в бизнесе.',
    price: '10 000р.',
    countPeople: '30 чел.',
  },
  {
    name: 'Корпоратив',
    image: png1,
    text: 'Традиция проведения организованного совместного отдыха сотрудников из одной компании уже прочно вошла в корпоративную культуру в России – с чем согласны все. Однако далеко не все в полной мере понимают и осознают, зачем нужны эти мероприятия. Ведь любой корпоратив – это прежде всего мощный инструмент HR-политики в бизнесе.',
    price: '10 000р.',
    countPeople: '30 чел.',
  },
  {
    name: 'Корпоратив',
    image: png1,
    text: 'Традиция проведения организованного совместного отдыха сотрудников из одной компании уже прочно вошла в корпоративную культуру в России – с чем согласны все. Однако далеко не все в полной мере понимают и осознают, зачем нужны эти мероприятия. Ведь любой корпоратив – это прежде всего мощный инструмент HR-политики в бизнесе.',
    price: '10 000р.',
    countPeople: '30 чел.',
  },
  {
    name: 'Корпоратив',
    image: png1,
    text: 'Традиция проведения организованного совместного отдыха сотрудников из одной компании уже прочно вошла в корпоративную культуру в России – с чем согласны все. Однако далеко не все в полной мере понимают и осознают, зачем нужны эти мероприятия. Ведь любой корпоратив – это прежде всего мощный инструмент HR-политики в бизнесе.',
    price: '10 000р.',
    countPeople: '30 чел.',
  },
  {
    name: 'Корпоратив',
    image: png1,
    text: 'Традиция проведения организованного совместного отдыха сотрудников из одной компании уже прочно вошла в корпоративную культуру в России – с чем согласны все. Однако далеко не все в полной мере понимают и осознают, зачем нужны эти мероприятия. Ведь любой корпоратив – это прежде всего мощный инструмент HR-политики в бизнесе.',
    price: '10 000р.',
    countPeople: '30 чел.',
  },
  {
    name: 'Корпоратив',
    image: png1,
    text: 'Традиция проведения организованного совместного отдыха сотрудников из одной компании уже прочно вошла в корпоративную культуру в России – с чем согласны все. Однако далеко не все в полной мере понимают и осознают, зачем нужны эти мероприятия. Ведь любой корпоратив – это прежде всего мощный инструмент HR-политики в бизнесе.',
    price: '10 000р.',
    countPeople: '30 чел.',
  },
]