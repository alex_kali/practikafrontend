import {FC} from "react";
import {EventOrganisationStyled} from "./styled";
import {LeftBlockStyled, RightBlockStyled, TextStyled, TitleStyled} from "../styled";
import {eventOrganisationsExample} from "./examples";
import {Event} from "./event";

export const EventOrganisation:FC = () => {
  return (
    <EventOrganisationStyled>
        <LeftBlockStyled>
          <TitleStyled>Организация праздников и мероприятий под ключ</TitleStyled>
          <TextStyled>Мы делаем процесс организации простым и незаметным для клиентов, помогаем экономить время и нервы.</TextStyled>
          <TextStyled>Широкий выбор программ, предлагаемых студией, впечатлит любого. Все предложения – это набор услуг, в котором можно выбирать мероприятия, ориентируясь на предпочтения, вкусы, масштабность и специфику мероприятия.</TextStyled>
        </LeftBlockStyled>
        <RightBlockStyled>
          {eventOrganisationsExample.map((item) => <Event event={item}/>)}
        </RightBlockStyled>
    </EventOrganisationStyled>
  )
}