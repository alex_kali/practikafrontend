import {FC, useState} from "react";
import {
  HomePagePageStyled,
  LeftBlockStyled,
  PromotionButtonStyled, PromotionLogoStyled,
  RightBlockStyled,
  ShowAllMenuStyled,
  TitleStyled
} from "./styled";
import {sliderHomeData} from "../../static/home/slider";
import {Slider} from "../../components/slider";
import {ViewAllStyled} from "../../components/slider/styled";
import {useNavigate, useParams} from "react-router-dom";

export const HomePage:FC = () => {
  const { page, position } = useParams()
  const navigate = useNavigate()
  return (
    <HomePagePageStyled position={position as string}>
      <LeftBlockStyled isView={!(page !== 'home')}>
        <TitleStyled>Эксклюзивное меню на Ваш вкус</TitleStyled>
        <ShowAllMenuStyled onClick={() => navigate('/center/authMenu')}>Смотреть все меню</ShowAllMenuStyled>
        <PromotionButtonStyled><PromotionLogoStyled/>АКЦИИ НА СЕГОДНЯ</PromotionButtonStyled>
      </LeftBlockStyled>

      <RightBlockStyled isView={!(page !== 'home')}>
        <Slider images={sliderHomeData}/>
        <ViewAllStyled isView={!(page !== 'home')} onClick={() => navigate('/center/authMenu')}>Смотреть все</ViewAllStyled>
      </RightBlockStyled>
    </HomePagePageStyled>
  )
}