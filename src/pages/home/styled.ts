import styled from "styled-components";
import {ReactComponent as fire} from './icons/fire.svg';

export const HomePagePageStyled = styled.div<{position: string}>`
  display: flex;
  width: 100%;
  height: 100%;
  max-height: 670px;
  ${props => props.position === 'bottom' ? 'height: 60px;' : ''}
`

export const LeftBlockStyled = styled.div<{isView: boolean}>`
  width: 700px;
  align-self: flex-end;
  overflow: hidden;
  transition-duration: 0.5s;
  ${props => props.isView ? '' : 'width:0 !important; opacity: 0;'}
`

export const TitleStyled = styled.div`
  margin-left: 60px;
  font-size: 64px;
  width: 560px;
  
  color: white;
  font-family: "LatoWebBold", sans-serif;
`

export const ShowAllMenuStyled = styled.div`
  margin-left: 60px;
  margin-bottom: 50px;
  margin-top: 50px;
  color: #FF9100;
  cursor: pointer;
  width: 310px;
  &:before{
    content: '';
    display: block;
    width: 100px;
    height: 2px;
    background: #FF9100;
    float: left;
    margin-top: 8px;
    margin-right: 26px;
  }
`

export const PromotionButtonStyled = styled.div`
  margin-bottom: 60px;
  width: 316px;
  height: 90px;
  background: #FF9100;
  display: flex;
  align-items: center;
  justify-content: center;
  border-top-right-radius: 20px;
  border-bottom-right-radius: 20px;
  font-family: "LatoWebBold", sans-serif;
`

export const PromotionLogoStyled = styled(fire)`
  margin-right: 15px;
`

export const RightBlockStyled = styled.div<{isView: boolean}>`
  width: calc(100% - 700px);
  transition-duration: 0.5s;
  ${props => props.isView ? '' : `
    width: 100%;
    .range{
      width: calc(100% - 60px);
      padding-left: 30px;
      padding-right: 30px;
    }
  `}
`