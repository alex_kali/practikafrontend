import {FC} from "react";
import {
  AboutAsStyled,
  AboutAsTextStyled,
  AboutAsTitleStyled, ContactTextStyled,
  ContactTitleStyled,
  ImageStyled,
  TitleStyled
} from "./styled";
import png1 from "./icons/1.jpg"

export const AboutAs:FC = () => {
  return (
    <AboutAsStyled>
      <ImageStyled style={{backgroundImage: `url(${png1})`}}/>
      <TitleStyled>Добро пожаловать на сайт ZIRA</TitleStyled>

      <AboutAsTitleStyled>About us</AboutAsTitleStyled>
      <AboutAsTextStyled>Nullam et orci eu lorem consequat tincidunt vivamus et sagittis libero. Mauris aliquet magna magna sed nunc rhoncus pharetra. Pellentesque condimentum sem. In efficitur ligula tate urna. Maecenas laoreet massa vel lacinia pellentesque lorem ipsum dolor. Nullam et orci eu lorem consequat tincidunt. Vivamus et sagittis libero. Mauris aliquet magna magna sed nunc rhoncus amet pharetra et feugiat tempus.</AboutAsTextStyled>

      <ContactTitleStyled>Contacts</ContactTitleStyled>
      <ContactTextStyled>
        Email: lexa29.03.1999@gmail.com
        <br/>
        Phone: +7 999 600 32 25
        <br/>
        address: sdfh sdf gfdg 2d
      </ContactTextStyled>
    </AboutAsStyled>
  )
}