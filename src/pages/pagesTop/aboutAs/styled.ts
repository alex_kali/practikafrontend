import styled from "styled-components";

export const AboutAsStyled = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
`

export const ImageStyled = styled.div`
  width: 100%;
  height: 100%;
  background: white;
  background-size: cover;
  background-position: center;
  position: relative;
  &:before{
    content: '';
    display: block;
    position: absolute;
    left: 0;
    bottom: 0;
    height: 100%;
    width: 100%;
    background: rgb(31,33,37);
    background: linear-gradient(0deg, rgba(31,33,37,1) 0%, rgba(31,33,37,0.95) 20%, rgba(31,33,37,0.9) 40%, rgba(31,33,37,0.85) 60%, rgba(31,33,37,0.8) 80%, rgba(31,33,37,0.75) 100%);
    z-index: 1;
    transition-duration: 0.5s;
  }
`

export const TitleStyled = styled.div`
  position: absolute;
  top: 5%;
  width: 100%;
  text-align: center;
  font-size: 52px;
  color: #FF9100;
  font-family: "LatoWebBold",sans-serif;
  z-index: 2;
`

export const AboutAsTitleStyled = styled.div`
  position: absolute;
  left: 5%;
  width: 30%;
  text-align: center;
  top: 20%;
  color: #73777D;
  z-index: 1;
  font-size: 24px;
`


export const AboutAsTextStyled = styled.div`
  position: absolute;
  left: 5%;
  width: 40%;
  top: 25%;
  color: #73777D;
  z-index: 1;
  font-size: 20px;
  line-height: 130%;
`

export const ContactTitleStyled = styled.div`
  position: absolute;
  right: 10%;
  width: 350px;
  text-align: center;
  top: 70%;
  color: #73777D;
  z-index: 1;
  font-size: 24px;
`

export const ContactTextStyled = styled.div`
  position: absolute;
  right: 10%;
  width: 350px;
  top: 75%;
  color: #73777D;
  z-index: 1;
  font-size: 20px;
  line-height: 150%;
`