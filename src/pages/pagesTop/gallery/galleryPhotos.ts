import png1 from "./icons/1.jpg"
import png2 from "./icons/2.jpg"
import png3 from "./icons/3.jpg"
import png4 from "./icons/4.jpg"
import png5 from "./icons/5.jpg"
import png6 from "./icons/6.jpg"
import png7 from "./icons/7.jpg"
import png8 from "./icons/8.jpg"

import png9 from "./icons/9.jpg"
import png10 from "./icons/10.jpg"
import png11 from "./icons/11.jpg"
import png12 from "./icons/12.jpg"
import png13 from "./icons/13.jpg"
import png14 from "./icons/14.jpg"
import png15 from "./icons/15.jpg"

export const galleryPhotos = [
  {
    src: png1,
    width: 800,
    height: 500
  },
  {
    src: png2,
    width: 800,
    height: 500
  },
  {
    src: png3,
    width: 800,
    height: 500
  },
  {
    src: png4,
    width: 800,
    height: 500
  },
  {
    src: png5,
    width: 600,
    height: 500
  },
  {
    src: png6,
    width: 600,
    height: 500
  },
  {
    src: png7,
    width: 600,
    height: 500
  },
  {
    src: png8,
    width: 600,
    height: 500
  },

  {
    src: png9,
    width: 600,
    height: 500
  },
  {
    src: png10,
    width: 600,
    height: 500
  },
  {
    src: png11,
    width: 600,
    height: 500
  },
  {
    src: png12,
    width: 600,
    height: 500
  },
  {
    src: png13,
    width: 600,
    height: 500
  },
  {
    src: png14,
    width: 600,
    height: 500
  },
  {
    src: png15,
    width: 600,
    height: 500
  },
]