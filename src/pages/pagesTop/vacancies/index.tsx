import {FC, useState} from "react";
import {
  BottomLeftBlockStyled,
  BottomRightBlockStyled,
  ContentStyled,
  TitleStyled,
  TopLeftBlockStyled,
  TopRightBlockStyled,
  VacanciesStyled,
} from "./styled";
import {VacanciesView} from "./vacancies";
import {Advantages} from "./advantages";
import {Reviews} from "./reviews";
import {Hiring} from "./hiring";

export const Vacancies:FC = () => {
  const [page, changePage] = useState('')

  return (
    <VacanciesStyled>
      <TopLeftBlockStyled page={page}>
        <TitleStyled onClick={()=>changePage('jobs')} className={'title'}>Вакансии</TitleStyled>
        <ContentStyled className={'content'}>
          <VacanciesView />
        </ContentStyled>
      </TopLeftBlockStyled>

      <TopRightBlockStyled page={page}>
        <TitleStyled onClick={()=>changePage('advantages')} className={'title'}>Принципы</TitleStyled>
        <ContentStyled className={'content'}>
          <Advantages/>
        </ContentStyled>
      </TopRightBlockStyled>

      <BottomLeftBlockStyled page={page}>
        <TitleStyled onClick={()=>changePage('reviews')} className={'title'}>Отзывы</TitleStyled>
        <ContentStyled className={'content'}>
          <Reviews />
        </ContentStyled>
      </BottomLeftBlockStyled>

      <BottomRightBlockStyled page={page}>
        <TitleStyled onClick={()=>changePage('hiring')} className={'title'}>Процесс найма</TitleStyled>
        <ContentStyled className={'content'}>
          <Hiring />
        </ContentStyled>
      </BottomRightBlockStyled>
    </VacanciesStyled>
  )
}