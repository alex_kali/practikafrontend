import {FC} from "react";
import {AdvantagesLeftStyled, AdvantagesRightStyled, AdvantagesTextStyled, AdvantagesTitleStyled} from "./styled";

export const Advantages:FC = () => {
  return (
    <>
      <AdvantagesLeftStyled>
        <AdvantagesTitleStyled style={{color: '#4b7f0b', borderTopColor: '#4b7f0b'}}>ГОСТЬ – ГЛАВНЫЙ ЧЕЛОВЕК</AdvantagesTitleStyled>
        <AdvantagesTextStyled>В НАШЕМ РЕСТОРАНЕ. ЦЕЛЬ НАШЕЙ РАБОТЫ – ПРЕВЗОЙТИ ЕГО ОЖИДАНИЯ.</AdvantagesTextStyled>

        <AdvantagesTitleStyled style={{color: '#931116', borderTopColor: '#931116'}}>МЫ УВАЖАЕМ ДРУГ ДРУГА</AdvantagesTitleStyled>
        <AdvantagesTextStyled>И ВСЕГДА ГОТОВЫ ПРОТЯНУТЬ РУКУ ПОМОЩИ И ПОДСТАВИТЬ ПЛЕЧО.</AdvantagesTextStyled>

        <AdvantagesTitleStyled style={{color: '#703c2e', borderTopColor: '#703c2e'}}>МЫ ЛЮБИМ ЖИЗНЬ</AdvantagesTitleStyled>
        <AdvantagesTextStyled>И С ОПТИМИЗМОМ ПРИНИМАЕМ ЕЕ ВЫЗОВЫ. НАМ ЛЕГКО ОБЩАТЬСЯ, ПОТОМУ ЧТО МЫ УМЕЕМ ДОГОВАРИВАТЬСЯ.</AdvantagesTextStyled>

      </AdvantagesLeftStyled>
      <AdvantagesRightStyled>
        <AdvantagesTitleStyled style={{color: '#827f59', borderTopColor: '#827f59'}}>СМЕЛОСТЬ СТАВИТЬ ВЫСОКИЕ ЦЕЛИ</AdvantagesTitleStyled>
        <AdvantagesTextStyled>И УПОРСТВО ИХ ДОСТИГАТЬ.</AdvantagesTextStyled>

        <AdvantagesTitleStyled style={{color: '#a36212', borderTopColor: '#a36212'}}>ПРИВЕТСТВУЕМ ИНИЦИАТИВУ</AdvantagesTitleStyled>
        <AdvantagesTextStyled>И САМОРАЗВИТИЕ. ОЦЕНИВАЕМ ЗАСЛУГИ. ВОЗНАГРАЖДАЕМ РЕЗУЛЬТАТ.</AdvantagesTextStyled>
      </AdvantagesRightStyled>
    </>
  )
}