import {FC, useState} from "react";
import {
  ButtonStyled,
  ColumnStyled, ItemColumnStyled,
  VacancyDescriptionStyled,
  VacancyExperienceStyled,
  VacancyPlaceStyled,
  VacancyStyled,
  VacancyTitleStyled,
  WrapperVacancyStyled
} from "./styled";

export const VacanciesView:FC = () => {
  const [idActive, setIdActive] = useState(0)
  return (
    <>
      <WrapperVacancyStyled isActive={idActive === 1} onClick={() => setIdActive(1)}>
        <VacancyStyled>
          <VacancyTitleStyled>МЕНЕДЖЕР-СТАЖЕР</VacancyTitleStyled>
          <VacancyPlaceStyled>г. Москва</VacancyPlaceStyled>
          <VacancyExperienceStyled>от одного года</VacancyExperienceStyled>
        </VacancyStyled>
        <VacancyDescriptionStyled>
          <ColumnStyled>
            <ItemColumnStyled>Лидерские качества</ItemColumnStyled>
            <ItemColumnStyled>Коммуникативные навыки</ItemColumnStyled>
            <ItemColumnStyled>Активность</ItemColumnStyled>
            <ItemColumnStyled>Позитивность</ItemColumnStyled>
            <ItemColumnStyled>Готовность следовать стандартам компании</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Организация работы ресторана</ItemColumnStyled>
            <ItemColumnStyled>Управление персоналом: обучение, адаптация, оценка работы</ItemColumnStyled>
            <ItemColumnStyled>Общение с гостями</ItemColumnStyled>
            <ItemColumnStyled>Выполнение целевых показателей</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Стабильный оклад + бонусы после окончания программы обучения</ItemColumnStyled>
            <ItemColumnStyled>Гибкий график</ItemColumnStyled>
            <ItemColumnStyled>Твои интересы – наш приоритет</ItemColumnStyled>
            <ItemColumnStyled>Стремительный карьерный рост до заместителя директора ресторана или директора ресторана "Бургер Кинг"</ItemColumnStyled>
            <ItemColumnStyled>Бесплатное обучение по лучшим международным стандартам</ItemColumnStyled>
          </ColumnStyled>
          <ButtonStyled>Заполнить анкету</ButtonStyled>
        </VacancyDescriptionStyled>
      </WrapperVacancyStyled>
      <WrapperVacancyStyled isActive={idActive === 2} onClick={() => setIdActive(2)}>
        <VacancyStyled>
          <VacancyTitleStyled>ЧЛЕН БРИГАДЫ РЕСТОРАНА (НОЧНАЯ СМЕНА)</VacancyTitleStyled>
          <VacancyPlaceStyled>г. Москва</VacancyPlaceStyled>
          <VacancyExperienceStyled>от одного года</VacancyExperienceStyled>
        </VacancyStyled>
        <VacancyDescriptionStyled>
          <ColumnStyled>
            <ItemColumnStyled>Лидерские качества</ItemColumnStyled>
            <ItemColumnStyled>Коммуникативные навыки</ItemColumnStyled>
            <ItemColumnStyled>Активность</ItemColumnStyled>
            <ItemColumnStyled>Позитивность</ItemColumnStyled>
            <ItemColumnStyled>Готовность следовать стандартам компании</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Организация работы ресторана</ItemColumnStyled>
            <ItemColumnStyled>Управление персоналом: обучение, адаптация, оценка работы</ItemColumnStyled>
            <ItemColumnStyled>Общение с гостями</ItemColumnStyled>
            <ItemColumnStyled>Выполнение целевых показателей</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Стабильный оклад + бонусы после окончания программы обучения</ItemColumnStyled>
            <ItemColumnStyled>Гибкий график</ItemColumnStyled>
            <ItemColumnStyled>Твои интересы – наш приоритет</ItemColumnStyled>
            <ItemColumnStyled>Стремительный карьерный рост до заместителя директора ресторана или директора ресторана "Бургер Кинг"</ItemColumnStyled>
            <ItemColumnStyled>Бесплатное обучение по лучшим международным стандартам</ItemColumnStyled>
          </ColumnStyled>
          <ButtonStyled>Заполнить анкету</ButtonStyled>
        </VacancyDescriptionStyled>
      </WrapperVacancyStyled>
      <WrapperVacancyStyled isActive={idActive === 3} onClick={() => setIdActive(3)}>
        <VacancyStyled>
          <VacancyTitleStyled>ЧЛЕН БРИГАДЫ РЕСТОРАНА</VacancyTitleStyled>
          <VacancyPlaceStyled>г. Москва</VacancyPlaceStyled>
          <VacancyExperienceStyled>от одного года</VacancyExperienceStyled>
        </VacancyStyled>
        <VacancyDescriptionStyled>
          <ColumnStyled>
            <ItemColumnStyled>Лидерские качества</ItemColumnStyled>
            <ItemColumnStyled>Коммуникативные навыки</ItemColumnStyled>
            <ItemColumnStyled>Активность</ItemColumnStyled>
            <ItemColumnStyled>Позитивность</ItemColumnStyled>
            <ItemColumnStyled>Готовность следовать стандартам компании</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Организация работы ресторана</ItemColumnStyled>
            <ItemColumnStyled>Управление персоналом: обучение, адаптация, оценка работы</ItemColumnStyled>
            <ItemColumnStyled>Общение с гостями</ItemColumnStyled>
            <ItemColumnStyled>Выполнение целевых показателей</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Стабильный оклад + бонусы после окончания программы обучения</ItemColumnStyled>
            <ItemColumnStyled>Гибкий график</ItemColumnStyled>
            <ItemColumnStyled>Твои интересы – наш приоритет</ItemColumnStyled>
            <ItemColumnStyled>Стремительный карьерный рост до заместителя директора ресторана или директора ресторана "Бургер Кинг"</ItemColumnStyled>
            <ItemColumnStyled>Бесплатное обучение по лучшим международным стандартам</ItemColumnStyled>
          </ColumnStyled>
          <ButtonStyled>Заполнить анкету</ButtonStyled>
        </VacancyDescriptionStyled>
      </WrapperVacancyStyled>
      <WrapperVacancyStyled isActive={idActive === 4} onClick={() => setIdActive(4)}>
        <VacancyStyled>
          <VacancyTitleStyled>ВОДИТЕЛЬ-КУРЬЕР НА ЛИЧНОМ АВТО</VacancyTitleStyled>
          <VacancyPlaceStyled>г. Москва</VacancyPlaceStyled>
          <VacancyExperienceStyled>от одного года</VacancyExperienceStyled>
        </VacancyStyled>
        <VacancyDescriptionStyled>
          <ColumnStyled>
            <ItemColumnStyled>Лидерские качества</ItemColumnStyled>
            <ItemColumnStyled>Коммуникативные навыки</ItemColumnStyled>
            <ItemColumnStyled>Активность</ItemColumnStyled>
            <ItemColumnStyled>Позитивность</ItemColumnStyled>
            <ItemColumnStyled>Готовность следовать стандартам компании</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Организация работы ресторана</ItemColumnStyled>
            <ItemColumnStyled>Управление персоналом: обучение, адаптация, оценка работы</ItemColumnStyled>
            <ItemColumnStyled>Общение с гостями</ItemColumnStyled>
            <ItemColumnStyled>Выполнение целевых показателей</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Стабильный оклад + бонусы после окончания программы обучения</ItemColumnStyled>
            <ItemColumnStyled>Гибкий график</ItemColumnStyled>
            <ItemColumnStyled>Твои интересы – наш приоритет</ItemColumnStyled>
            <ItemColumnStyled>Стремительный карьерный рост до заместителя директора ресторана или директора ресторана "Бургер Кинг"</ItemColumnStyled>
            <ItemColumnStyled>Бесплатное обучение по лучшим международным стандартам</ItemColumnStyled>
          </ColumnStyled>
          <ButtonStyled>Заполнить анкету</ButtonStyled>
        </VacancyDescriptionStyled>
      </WrapperVacancyStyled>
      <WrapperVacancyStyled isActive={idActive === 5} onClick={() => setIdActive(5)}>
        <VacancyStyled>
          <VacancyTitleStyled>ПЕШИЙ КУРЬЕР</VacancyTitleStyled>
          <VacancyPlaceStyled>г. Москва</VacancyPlaceStyled>
          <VacancyExperienceStyled>от одного года</VacancyExperienceStyled>
        </VacancyStyled>
        <VacancyDescriptionStyled>
          <ColumnStyled>
            <ItemColumnStyled>Лидерские качества</ItemColumnStyled>
            <ItemColumnStyled>Коммуникативные навыки</ItemColumnStyled>
            <ItemColumnStyled>Активность</ItemColumnStyled>
            <ItemColumnStyled>Позитивность</ItemColumnStyled>
            <ItemColumnStyled>Готовность следовать стандартам компании</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Организация работы ресторана</ItemColumnStyled>
            <ItemColumnStyled>Управление персоналом: обучение, адаптация, оценка работы</ItemColumnStyled>
            <ItemColumnStyled>Общение с гостями</ItemColumnStyled>
            <ItemColumnStyled>Выполнение целевых показателей</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Стабильный оклад + бонусы после окончания программы обучения</ItemColumnStyled>
            <ItemColumnStyled>Гибкий график</ItemColumnStyled>
            <ItemColumnStyled>Твои интересы – наш приоритет</ItemColumnStyled>
            <ItemColumnStyled>Стремительный карьерный рост до заместителя директора ресторана или директора ресторана "Бургер Кинг"</ItemColumnStyled>
            <ItemColumnStyled>Бесплатное обучение по лучшим международным стандартам</ItemColumnStyled>
          </ColumnStyled>
          <ButtonStyled>Заполнить анкету</ButtonStyled>
        </VacancyDescriptionStyled>
      </WrapperVacancyStyled>
    </>
  )
}