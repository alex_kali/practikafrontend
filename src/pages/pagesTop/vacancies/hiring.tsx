import {FC} from "react";
import {HiringStyled, ItemHiringStyled} from "./styled";

export const Hiring:FC = () => {
  return (
    <HiringStyled>
      <ItemHiringStyled>Отправка анкеты</ItemHiringStyled>
      <ItemHiringStyled>Собеседование</ItemHiringStyled>
      <ItemHiringStyled>Обучение</ItemHiringStyled>
      <ItemHiringStyled>Подписание договора</ItemHiringStyled>
    </HiringStyled>
  )
}