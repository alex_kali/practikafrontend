import styled from "styled-components";
const active = `
 & .title{
   color: #FF9100;
   height: 12%;
 }
 & .content{
   opacity: 1;
   margin-top: 0;
   overflow-y: auto;
 }
`

export const VacanciesStyled = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
`

export const TopLeftBlockStyled = styled.div<{page: string}>`
  overflow: hidden;
  width: calc(50% - 1px);
  height: calc(50% - 1px);
  border-right: 1px solid #36393E;
  border-bottom: 1px solid #36393E;
  transition-duration: 0.5s;
  float: left;
  ${props => {
    if(props.page === 'jobs'){
      return 'width: calc(80% - 1px);height: calc(80% - 1px);' + active
    }else if(props.page === 'advantages'){
      return 'width: calc(20% - 1px);height: calc(80% - 1px);'
    }else if(props.page === 'reviews'){
      return 'height: calc(20% - 1px);'
    }else if(props.page === 'hiring'){
      return 'height: calc(20% - 1px);'
    }else{
      return ''
    }
  }}
`

export const TopRightBlockStyled = styled.div<{page: string}>`
  overflow: hidden;
  width: calc(50% - 1px);
  height: calc(50% - 1px);
  border-left: 1px solid #36393E;
  border-bottom: 1px solid #36393E;
  transition-duration: 0.5s;
  float: left;
  ${props => {
    if(props.page === 'jobs'){
      return 'width: calc(20% - 1px);height: calc(80% - 1px);'
    }else if(props.page === 'advantages'){
      return 'width: calc(80% - 1px);height: calc(80% - 1px);' + active
    }else if(props.page === 'reviews'){
      return 'height: calc(20% - 1px);'
    }else if(props.page === 'hiring'){
      return 'height: calc(20% - 1px);'
    }else{
      return ''
    }
  }}
`

export const BottomLeftBlockStyled = styled.div<{page: string}>`
  overflow: hidden;
  width: calc(50% - 1px);
  height: calc(50% - 1px);
  border-right: 1px solid #36393E;
  border-top: 1px solid #36393E;
  transition-duration: 0.5s;
  float: left;
  ${props => {
    if(props.page === 'jobs'){
      return 'height: calc(20% - 1px);'
    }else if(props.page === 'advantages'){
      return 'height: calc(20% - 1px);'
    }else if(props.page === 'reviews'){
      return 'width: calc(80% - 1px);height: calc(80% - 1px);' + active
    }else if(props.page === 'hiring'){
      return 'height: calc(80% - 1px);width: calc(20% - 1px);'
    }else{
      return ''
    }
  }}
`

export const BottomRightBlockStyled = styled.div<{page: string}>`
  overflow: hidden;
  width: calc(50% - 1px);
  height: calc(50% - 1px);
  border-left: 1px solid #36393E;
  border-top: 1px solid #36393E;
  transition-duration: 0.5s;
  float: left;
  ${props => {
    if(props.page === 'jobs'){
      return 'height: calc(20% - 1px);'
    }else if(props.page === 'advantages'){
      return 'height: calc(20% - 1px);'
    }else if(props.page === 'reviews'){
      return 'height: calc(80% - 1px);width: calc(20% - 1px);'
    }else if(props.page === 'hiring'){
      return 'width: calc(80% - 1px);height: calc(80% - 1px);' + active
    }else{
      return ''
    }
  }}
`

export const TitleStyled = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #73777D;
  font-size: 24px;
  cursor: pointer;
  transition-duration: 0.5s;
`

export const ContentStyled = styled.div`
  height: calc(90% - 20px);
  width: calc(80vw - 10px);
  transition-duration: 0.5s;
  opacity: 0;
  margin-top: 100vh;
  padding-right: 5px;
  margin-bottom: 20px;
  position: relative;
`

export const WrapperVacancyStyled = styled.div<{isActive: boolean}>`
  width: calc(90% - 4px);
  height: 80px;
  margin-bottom: 15px;
  max-width: 1000px;
  margin-left: auto;
  margin-right: auto;
  padding-left: 20px;
  padding-right: 20px;
  color: #73777D;
  border: 2px solid #36393E;
  cursor: pointer;
  overflow: hidden;
  transition-duration: 0.5s;
  ${props => props.isActive ? 'height: 450px;' : ''}
`

export const VacancyStyled = styled.div`
  width: 100%;
  height: 80px;
  display: flex;
  align-items: center;
`

export const VacancyDescriptionStyled = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: space-between;
  margin-left: auto;
  margin-right: auto;
  max-width: 900px;
  padding-left: 30px;
  padding-right: 30px;
  position: relative;
  height: 350px;
`

export const ColumnStyled = styled.div`
  
`

export const ItemColumnStyled = styled.div`
  width: 260px;
  margin-top: 10px;
  margin-bottom: 10px;
  position: relative;
  &:before{
    content: '';
    width: 8px;
    height: 8px;
    display: block;
    background: #FF9100;
    position: absolute;
    left: -14px;
    border-radius: 20px;
    top: calc(50% - 4px);
  }
`

export const ButtonStyled = styled.div`
  position: absolute;
  right: 0px;
  bottom: 20px;
  cursor: pointer;
  border: 2px solid #73777D;
  padding: 10px;
  padding-left: 15px;
  padding-right: 15px;
  border-radius: 8px;
  transition-duration: 0.5s;
  &:hover{
    color: #FF9100;
    border-color: #FF9100;
  }
`

export const VacancyTitleStyled = styled.div`
  width: 50%;
  position: relative;
  &:before{
    content: 'Должность:';
    color: #FF9100;
    display: block;
    font-size: 12px;
    font-family: "LatoWebLight",sans-serif;
    margin-bottom: 3px;
  }
`

export const VacancyPlaceStyled = styled.div`
  width: 25%;
  position: relative;
  &:before{
    content: 'Место работы:';
    color: #FF9100;
    display: block;
    font-size: 12px;
    font-family: "LatoWebLight",sans-serif;
    margin-bottom: 3px;
  }
`

export const VacancyExperienceStyled = styled.div`
  width: 25%;
  position: relative;
  &:before{
    content: 'Опыт работы:';
    color: #FF9100;
    display: block;
    font-size: 12px;
    font-family: "LatoWebLight",sans-serif;
    margin-bottom: 3px;
  }
`

export const AdvantagesLeftStyled = styled.div`
  width: 430px;
  height: calc(100% - 15px);
  float: left;
  padding-left: 30px;
  margin-top: 15px;
`

export const AdvantagesRightStyled = styled.div`
  width: 430px;
  height: calc(100% - 15px);
  float: right;
  padding-right: 30px;
  margin-top: 15px;
`

export const AdvantagesTitleStyled = styled.div`
  border-top: 2px solid;
  font-size: 22px;
  padding-top: 15px;
  padding-bottom: 15px;
  padding-left: 5px;
`

export const AdvantagesTextStyled = styled.div`
  margin-bottom: 32px;
  color: #73777D;
  line-height: 150%;
  font-size: 16px;
  padding-left: 5px;
`

export const ReviewsStyled = styled.div`
  margin-left: 2%;
  width: calc(50% - 3% - 45px);
  float: left;
  border: 2px solid #36393E;
  margin-bottom: 20px;
  padding-left: 30px;
  padding-top: 20px;
  padding-right: 15px;
  padding-bottom: 30px;
`

export const ReviewsTitleStyled = styled.div`
  color: #73777D;
  font-size: 26px;
`

export const ReviewsWorkStyled = styled.div`
  margin-top: 10px;
  color: #FF9100;
  font-size: 12px;
  font-family: "LatoWebLight",sans-serif;
`

export const ReviewsTextStyled = styled.div`
  margin-top: 15px;
  color: #73777D;
  font-size: 18px;
`

export const HiringStyled = styled.div`
  display: flex;
  justify-content: space-between;
  position: absolute;
  top: 35%;
  width: calc(100% - 40px);
  padding-left: 20px;
  padding-right: 20px;
  &:before{
    content: '';
    display: block;
    top: calc(50% - 1px);
    height: 2px;
    left: 30px;
    width: calc(100% - 60px);
    background: #73777D;
    position: absolute;
    z-index: -1;
  }
`

export const ItemHiringStyled = styled.div`
  color: #73777D;
  font-size: 18px;
  padding: 20px;
  background: #1F2125;
  border: 2px solid #73777D;
  border-radius: 8px;
  cursor: pointer;
`