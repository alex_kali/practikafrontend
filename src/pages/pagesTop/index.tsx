import {AboutAs} from "./aboutAs";
import {Blog} from "./blog";
import {Gallery} from "./gallery";
import {Vacancies} from "./vacancies";
import {Events} from "./events";

export const PagesTop:any = {
  aboutAs: {
    component: <AboutAs />,
    position: 1,
  },
  gallery: {
    component: <Gallery />,
    position: 2,
  },
  blog: {
    component: <Blog />,
    position: 3,
  },
  vacancies: {
    component: <Vacancies />,
    position: 4,
  },
  events: {
    component: <Events />,
    position: 5,
  },
}