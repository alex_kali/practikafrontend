import React from "react";
import {AuthForm} from "./login";

export const RightPages:any = {
  login: {
    component: <AuthForm />,
    position: 1,
  },
  registration: {
    component: <AuthForm />,
    position: 1,
  },
}