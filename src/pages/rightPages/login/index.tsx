import React, {FC} from "react";
import {
  AuthFormStyled,
  TitleStyled,
  WrapperFormStyled,
  WrapperLoginStyled,
  WrapperRegistrationStyled,
  WrapperTitleStyled
} from "./styled";
import {useNavigate, useParams} from "react-router-dom";
import {Registration} from "./registration";
import {Login} from "./login";

export const AuthForm:FC = () => {
  const navigate = useNavigate()
  const { page } = useParams()
  return (
    <AuthFormStyled>
      <WrapperFormStyled style={{height: page === 'registration' ? '510px' : '320px'}}>
        <WrapperTitleStyled>
          <TitleStyled isActive={page === 'login'} onClick={()=>navigate('/right/login')}>Авторизация</TitleStyled>
          <TitleStyled isActive={page === 'registration'} onClick={()=>navigate('/right/registration')} style={{borderLeft: '2px solid #36393E'}}>Регистрация</TitleStyled>
        </WrapperTitleStyled>

        <WrapperLoginStyled isActive={page !== 'registration'}>
          <Login/>
        </WrapperLoginStyled>

        <WrapperRegistrationStyled isActive={page === 'registration'}>
          <Registration/>
        </WrapperRegistrationStyled>
      </WrapperFormStyled>
    </AuthFormStyled>
  )
}