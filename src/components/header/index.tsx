import {FC} from "react";
import {
  HeaderStyled,
  InstagramStyled,
  LogoStyled,
  PageLinksStyled,
  PageLinkStyled,
  SocialLinksStyled,
  SocialLinkStyled,
  TelegramStyled,
  TwitterStyled,
  UserIconStyled,
  UserLinksStyled,
  UsernameStyled,
  VkStyled,
  WrapperLinksStyled,
  WrapperLogoStyled
} from "./styled";
import {useNavigate, useLocation, useParams} from "react-router-dom";
import {User} from "../../store/user/model";
import {useToken} from "../../store/user/useToken";

export const Header:FC = () => {
  const location = useLocation()
  const navigate = useNavigate()

  const { position, page } = useParams()

  const [, setUserToken] = useToken()
  const userData = User.useData()

  return (
    <HeaderStyled position={position as string}>
      <WrapperLogoStyled page={page as string}>
        <LogoStyled page={page as string}/>
      </WrapperLogoStyled>

      <WrapperLinksStyled position={position as string}>
        <SocialLinksStyled position={position as string}>
          <InstagramStyled/>
          <TelegramStyled/>
          <TwitterStyled/>
          <VkStyled/>
        </SocialLinksStyled>

        <PageLinksStyled>
          <PageLinkStyled onClick={()=>navigate('/center/home')} isActive={location.pathname === '/center/home'}>ГЛАВНАЯ</PageLinkStyled>
          <PageLinkStyled onClick={()=>navigate('/center/menu')} isActive={location.pathname === '/center/menu'}>МЕНЮ</PageLinkStyled>
          <PageLinkStyled onClick={()=>navigate('/top/aboutAs')} isActive={location.pathname === '/top/aboutAs'}>О НАС</PageLinkStyled>
          <PageLinkStyled onClick={()=>navigate('/top/gallery')} isActive={location.pathname === '/top/gallery'}>ГАЛЕРЕЯ</PageLinkStyled>
          {/*<PageLinkStyled onClick={()=>navigate('/top/blog')} isActive={location.pathname === '/top/blog'}>БЛОГ</PageLinkStyled>*/}
          <PageLinkStyled onClick={()=>navigate('/top/vacancies')} isActive={location.pathname === '/top/vacancies'}>ВАКАНСИИ</PageLinkStyled>
          {/*<PageLinkStyled onClick={()=>navigate('/top/events')} isActive={location.pathname === '/top/events'}>МЕРОПРИЯТИЯ</PageLinkStyled>*/}
        </PageLinksStyled>

        <UserLinksStyled onClick={()=> {
          userData ? navigate('/right/userPage') : navigate('/right/login')
        }}>
          {userData ? <><UserIconStyled/><UsernameStyled>+{userData.username}</UsernameStyled></> : 'ВХОД / РЕГИСТРАЦИЯ'}
        </UserLinksStyled>


      </WrapperLinksStyled>
    </HeaderStyled>
  )
}