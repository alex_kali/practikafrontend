import styled from "styled-components";
import {ReactComponent as Logo} from './icons/logo.svg';
import {ReactComponent as instagram} from './icons/instagram.svg';
import {ReactComponent as telegram} from './icons/telegram.svg';
import {ReactComponent as twiter} from './icons/twiter.svg';
import {ReactComponent as vk} from './icons/vk.svg';
import {ReactComponent as user} from './icons/user.svg';

export const HeaderStyled = styled.div<{position: string}>`
  height: 135px;
  width: 100%;
  padding-top: 1px;
  display: flex;
  margin-bottom: 10px;
  transition-duration: 0.5s;
  ${props => props.position === 'bottom' ? `height: 85px;` : ``}
`

export const WrapperLogoStyled = styled.div<{page: string}>`
  width: 230px;
  padding-right: 30px;
  padding-left: 10px;
  transition-duration: 0.5s;
  ${props => props.page !== 'home' ? `width: 150px;` : ``}
`

export const LogoStyled = styled(Logo)<{page: string}>`
  height: 99px;
  width: 204px;
  margin-left: calc(50% - 95px);
  margin-top: 32px;
  transition-duration: 0.5s;
  ${props => props.page  !== 'home' ? `margin-top: 5px; height: 60px; width: 120px; margin-left: calc(50% - 50px);` : ``}
`

export const WrapperLinksStyled = styled.div<{position: string}>`
  padding-left: calc(100% - 1500px);
  flex: 1 1 1280px;
  height: 76px;
  border-bottom: 2px solid #36393E;
  display: flex;
  align-items: center;
  justify-content: space-between;
  color: #73777D;
  ${props => props.position === 'top' ? `border-top: 2px solid #36393E; border-bottom: 2px solid rgba(0,0,0,0);` : ``}
`

export const SocialLinksStyled = styled.div<{position: string}>`
  display: flex;
  justify-content: space-between;
  width: 150px;
  padding-left: 20px;
  transition-duration: 0.5s;
`

export const SocialLinkStyled = styled.div`
  background: grey;
  width: 24px;
  height: 24px;
  border-radius: 50%;
`

export const InstagramStyled = styled(instagram)`
  width: 24px;
  height: 24px;
  border-radius: 50%;
  cursor: pointer;
  path{
    transition-duration: 0.3s;
  }
  &:hover{
    path{
      fill: #FF9100;
    }
  }
`

export const TelegramStyled = styled(telegram)`
  width: 24px;
  height: 24px;
  border-radius: 50%;
  cursor: pointer;
  rect{
    transition-duration: 0.3s;
  }
  &:hover{
    rect{
      fill: #FF9100;
    }
  }
`

export const TwitterStyled = styled(twiter)`
  width: 24px;
  height: 24px;
  border-radius: 50%;
  cursor: pointer;
  path{
    transition-duration: 0.3s;
  }
  &:hover{
    path{
      fill: #FF9100;
    }
  }
`

export const VkStyled = styled(vk)`
  width: 24px;
  height: 24px;
  border-radius: 50%;
  cursor: pointer;
  path{
    transition-duration: 0.3s; 
  }
  &:hover{
    path{
      fill: #FF9100;
    }
  }
`

export const PageLinksStyled = styled.div`
  display: flex;
  justify-content: space-between;
  flex: 2 2 800px;
  max-width: 800px;
  margin-left: 50px;
  margin-right: 50px;
`

export const PageLinkStyled = styled.div<{isActive?: boolean}>`
  cursor: pointer;
  transition-duration: 0.3s;
  ${props => props.isActive ? 'color: #FF9100;' : ''}
  &:hover{
    color: #FF9100; 
  }
`

export const UserLinksStyled = styled.div`
  flex: 0 0 180px;
  width: 180px;
  margin-right: 30px;
  cursor: pointer;
  transition-duration: 0.3s;
  position: relative;
  &:hover{
    color: #FF9100;
    path{
      fill: #FF9100;
    }
  }
`

export const UserIconStyled = styled(user)`
  width: 40px;
  height: 40px;
  path{
    transition-duration: 0.3s;
    fill: #73777D;
  }
`

export const UsernameStyled = styled.div`
  position: absolute;
  left: 46px;
  top: 15px;
`