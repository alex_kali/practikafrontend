import React, {FC, useEffect} from "react";
import {ArrowStyled, AuthMenuStyled, ItemMenuStyled} from "./styled";
import {useNavigate, useParams} from "react-router-dom";
import {User} from "../../store/user/model";

export const AuthMenu:FC = () => {
  const navigate = useNavigate()
  const { page } = useParams()
  const userData = User.useData()

  useEffect(() => {
    if((page === 'login' || page === 'registration') && userData){
      navigate('/right/userPage')
    }
  },[page,userData])

  return (
    <AuthMenuStyled>
      <ItemMenuStyled isActive={true} onClick={()=>navigate('/center/menu')}>
        <ArrowStyled/>
      </ItemMenuStyled>
      {!userData &&
        <>
          <ItemMenuStyled isActive={page === 'login'} onClick={()=>navigate('/right/login')}>
            Авторизация
          </ItemMenuStyled>

          <ItemMenuStyled isActive={page === 'registration'} onClick={()=>navigate('/right/registration')}>
            Регистрация
          </ItemMenuStyled>
        </>
      }
      {userData &&
      <>
        <ItemMenuStyled isActive={page === 'userPage'} onClick={()=>navigate('/right/userPage')}>
          Профиль
        </ItemMenuStyled>
        <ItemMenuStyled isActive={false} onClick={()=> {
          User.options.logout();
          navigate('/right/login')
        }}>
          Выйти
        </ItemMenuStyled>
      </>
      }

    </AuthMenuStyled>
  )
}