import styled from "styled-components";
import {ReactComponent as Arrow} from "./icons/arrow.svg";

export const AuthMenuStyled = styled.div`
  height: 100%;
  width: 160px;
  border-right: 2px solid #36393E;
  float: left;
`

export const ArrowStyled = styled(Arrow)`
  color: #73777D !important;
  width: 76px;
  height: 44px;
  transform: rotate(180deg);
  path{
    fill: #73777D !important;
  }
`

export const ItemMenuStyled = styled.div<{isActive: boolean}>`
  height: 80px;
  border-bottom: 2px solid #36393E;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  transition-duration: 0.3s;
  color: #73777D;
  & path{
    transition-duration: 0.3s;
  }
  &:hover{
    color: #FF9100;
    & path{
      fill: #FF9100 !important;
    }
    background: #2c2d30;
  }
  
  ${props => props.isActive ? 'color: #FF9100;' : ''}
`