import png1 from "./icons/1.png"
import png2 from "./icons/2.png"
import png3 from "./icons/3.png"
import png4 from "./icons/4.jpg"
import png5 from "./icons/5.jpg"
import png6 from "./icons/6.jpg"
import png7 from "./icons/7.jpg"
import png8 from "./icons/8.jpg"
import png9 from "./icons/9.jpg"

export const sliderHomeData = [
  {
    url: png1,
    name: 'Мероприятия',
    urlPage: '/bottom/eventOrganisation',
    pageName: 'eventOrganisation',
  },
  {
    url: png8,
    name: 'Доставка',
    urlPage: '/bottom/delivery',
    pageName: 'delivery',
  },
  {
    url: png9,
    name: 'Бронирование',
    urlPage: '/bottom/booking',
    pageName: 'booking',
  },
  {
    url: png2,
    name: 'Мясные блюда',
    urlPage: '/bottom/meatDishes',
    pageName: 'meatDishes',
  },
  {
    url: png3,
    name: 'Холодные блюда',
    urlPage: '/bottom/coldDishes',
    pageName: 'coldDishes',
  },
  {
    url: png4,
    name: 'Десерты',
    urlPage: '/bottom/desserts',
    pageName: 'desserts',
  },
  {
    url: png5,
    name: 'Алкогольные напитки',
    urlPage: '/bottom/alcoholicDrinks',
    pageName: 'alcoholicDrinks',
  },
  {
    url: png6,
    name: 'Горячие напитки',
    urlPage: '/bottom/hotDrinks',
    pageName: 'hotDrinks',
  },
  {
    url: png7,
    name: 'Холодные напитки',
    urlPage: '/bottom/coldDrinks',
    pageName: 'coldDrinks',
  },
]