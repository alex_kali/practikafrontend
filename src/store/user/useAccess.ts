import {useNavigate, useLocation} from "react-router-dom";
import {useEffect} from "react";

interface IUseAccess {
  isLoginUser: boolean | undefined;
  paths: Array<string>; //forbidden paths for authorized user, allowed paths for an unauthorized user
  redirectAuthorized: string;
}

export const useAccess = (props:IUseAccess) => {
  const location = useLocation()
  const navigate = useNavigate();

  useEffect(()=>{
    const isRedirect = props.paths.includes(location.pathname)

    if(props.isLoginUser){
      if(isRedirect){
        navigate(props.redirectAuthorized)
      }
    }else{

    }
  },[location,props.isLoginUser])
}