import {AppThunk} from "../store";
import {logoutUser, setUser} from "./slices";
import {ILogin, IRegistration} from "./model";
import {requestController} from "react-redux-request-controller";

export const getUserData = (): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: 'getUserData',
    url: '/user/get_user_data/',
    method: 'GET',
    action: setUser
  }))
};

export const login = (data:ILogin): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: 'login',
    params: data,
    url: '/user/login/',
    method: 'POST',
    action: setUser
  }))
};

export const registration = (data:IRegistration): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: 'registration',
    params: data,
    url: '/user/registration/',
    method: 'POST',
    action: setUser
  }))
};

export const logout = (): AppThunk => async (
  dispatch: any
) => {
  dispatch(logoutUser())
};