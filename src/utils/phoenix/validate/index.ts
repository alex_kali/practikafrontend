import {stringMin} from "./stringMin";
import {stringMax} from "./stringMax";
import {stringEmail} from "./stringEmail";
import {numberMin} from "./numberMin";
import {numberMax} from "./numberMax";


interface INumberValidate {
  data: any;
  messageError: string;
  isValidate: boolean;

  min: (value: any, messageError?: string) => INumberValidate;
  max: (value: any, messageError?: string) => INumberValidate;
}

export interface IStringValidate {
  data: any;
  messageError: string;
  isValidate: boolean;

  min: (value: any, messageError?: string) => IStringValidate;
  max: (value: any, messageError?: string) => IStringValidate;
  email: (messageError?: string) => IStringValidate;
}

export const validate = (data: string | number) => {
  return ({
    data: data,
    messageError: '',
    isValidate: true,
    string: function(this: any) {

      this.min = stringMin
      this.max = stringMax
      this.email = stringEmail
      return this as IStringValidate
    },
    number: function(this: any) {

      this.min = numberMin
      this.max = numberMax
      return this as INumberValidate
    },
  })
}