import {IStringValidate} from "./index";

export const stringMax = function(this: IStringValidate, value: number, messageError?:string ) {
  if(this.data.length > value){
    this.isValidate = false
    this.messageError+= messageError || `Максимальная длинна поля ${value}. `
  }
  return this
}