export const numberMin = function(this: any, value: number, messageError?:string ) {
  delete this.min
  if(this.data > value){
    this.isValidate = false
    this.messageError+= messageError || `Минимальное значение ${value}. `
  }
  return this
}