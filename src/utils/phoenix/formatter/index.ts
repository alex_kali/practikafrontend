import {onlyNumber} from "./formatterOnlyNumber";
import {formatterPrice} from "./formatterPrice";

export interface IStringFormatter {
  data: any;

  onlyNumber: () => IStringFormatter;
  price: () => IStringFormatter;
}

export const formatter = (data: string | number) => {
  return ({
    data: data,
    string: function(this: any) {
      this.price = formatterPrice
      this.onlyNumber = onlyNumber

      return this as IStringFormatter
    }
  })
}