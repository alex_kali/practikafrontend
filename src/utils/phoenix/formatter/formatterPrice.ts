export const formatterPrice = function(this: any) {
  this.data = this.data.replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + ',')
  return this
}