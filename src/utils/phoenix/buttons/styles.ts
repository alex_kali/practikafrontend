import styled from "styled-components";
import {BORDER_INPUT, BORDER_RADIUS_INPUT, COLOR_INPUT, OPACITY_FOCUS_INPUT, OPACITY_INPUT} from "../styles";

interface ISubmitButtonStyled {
  isValidForm: boolean;
}

export const SubmitButtonStyled = styled.button<ISubmitButtonStyled>`
  height: 46px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: auto;
  cursor: pointer;
  border: ${BORDER_INPUT};
  border-radius: ${BORDER_RADIUS_INPUT};
  margin-top: 30px;
  font-size: 18px;
  padding-left: 30px;
  padding-right: 30px;
  color: ${COLOR_INPUT};
  &:hover{
    background: rgb(0,0,0);
    border-color: ${COLOR_INPUT};
    color: ${COLOR_INPUT};
  }
  ${props => `
    opacity: ${props.isValidForm ? OPACITY_FOCUS_INPUT : OPACITY_INPUT};
    &:hover{
      cursor: ${props.isValidForm ? '' : 'not-allowed'};
    }
  `}
`
