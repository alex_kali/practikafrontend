import React, { FC, memo, useContext} from 'react';
import {formNameContext, useIsValidForm, onSubmitContext, onSubmitForm} from 'react-redux-hook-form';
import {SubmitButtonStyled} from "./styles";


interface ISubmitButton {
  text: string;
  className?: string;
}

export const SubmitButton: FC<ISubmitButton> = (props) => {
  const formName = useContext(formNameContext)
  const isValidForm = useIsValidForm(formName)
  const onSubmit = useContext(onSubmitContext)

  const onClick = () => {
    if(isValidForm){
      onSubmitForm(formName, onSubmit)
    }
  }
  return (
    <SubmitButtonStyled className={props.className} isValidForm={isValidForm} onClick={onClick}>
      {props.text}
    </SubmitButtonStyled>
  )
};
