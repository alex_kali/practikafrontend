import styled from "styled-components";
import {ReactComponent as check} from "./icons/check.svg";

export const WrapperCheckboxInputStyled = styled.div`
  
`

export const CheckboxInputStyled = styled.input`
  display: none;
`

interface IWrapperCheckboxItemStyled{
  checked: boolean;
}

export const WrapperCheckboxItemStyled = styled.div<IWrapperCheckboxItemStyled>`
  display: flex;
  justify-content: space-between;
  flex-direction: row-reverse;
  align-items: center;
  width: 400px;
  max-width: 100%;
  clear: both;
  margin-top: 4px;
  margin-bottom: 4px;
  transition-duration: 0.1s;
  cursor: pointer;
  input:checked ~ .checkbox{
    & > *{
      height: 14px;
    }
  }
  ${props => props.checked ? 'opacity: 1;' : 'opacity: 0.4;' }
`

export const CheckboxTextStyled = styled.div`
  margin-left: 15px;
  width: calc(100% - 50px);
  font-size: 16px;
`

export const CustomRadioStyled = styled.div`
  width: 14px;
  height: 14px;
  float: left;
  border: 2px solid grey;
  margin-left: 15px;
`

export const CustomCheckStyled = styled(check)`
  width: 14px;
  height: 0px;
  transition-duration: 0.1s;
`