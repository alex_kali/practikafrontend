import {FC, useEffect, useState} from "react";
import { useField } from "react-redux-hook-form";
import {
  MinusStyled,
  OptionsItemStyled,
  OptionsStyled,
  PlusStyled,
  TitleStyled,
  ValueStyled,
  WrapperSelectStyled
} from "./styled";

interface IData {
  text: string,
  value: string,
  children?: Array<IData>,
}

interface ICascaderInput {
  name: string;
  data: Array<IData>;
  title: string;
  required?: boolean;
  onChange?: (value: string) => void;
}

export const CascaderInput: FC<ICascaderInput> = (props) => {
  const [isView, setIsView] = useState(false)

  const {useData} = useField({
    name: props.name,
    isRequired: props.required,
  })

  const [data, changeData] = useData()

  return (
    <WrapperSelectStyled>
      <TitleStyled>{props.title}:</TitleStyled>
      <ValueStyled onClick={()=>{setIsView(!isView)}} onMouseLeave={()=>{setIsView(false)}} isView={isView}>
        {data && data.map((text: string)=> <>{text} </>)}
        <OptionsStyled style={{display: isView ? 'block' : 'none'}}>
          {props.data.map((i)=>
            <SelectItem key={i.value} item={i} changeData={changeData} iteration={1} way={[i.value]} checked={data ? data.includes(i.value) : false} data={data || []}/>
          )}
        </OptionsStyled>
      </ValueStyled>
    </WrapperSelectStyled>
  )
}

interface ISelectItem {
  item: IData;
  changeData: (data: any) => void;
  iteration: number;
  data: Array<string>;
  way: Array<string>;
  checked: boolean;
}

const SelectItem: FC<ISelectItem> = ({item, iteration, changeData,checked, way, data}) => {
  const [isView, setIsView] = useState(false)
  return (
    <>
      <OptionsItemStyled key={item.value} checked={checked} onClick={()=>{changeData(way)}} style={{paddingLeft: `${15 * iteration}px`}}>
        {item.children && isView && <MinusStyled onClick={(event)=>{setIsView(!isView);event.stopPropagation()}}/>}
        {item.children && !isView && <PlusStyled onClick={(event)=>{setIsView(!isView);event.stopPropagation()}}/>}
        {item.text}
      </OptionsItemStyled>
      {item.children && isView && item.children.map((i)=>
        <SelectItem
          key={i.value}
          item={i}
          iteration={iteration + 1}
          changeData={changeData}
          way={[...way, i.value]}
          data={data}
          checked={ !checked && iteration !== 1 ? false : iteration >= data.length? checked : data.includes(i.value)}
        />
      )}
    </>
  )
}