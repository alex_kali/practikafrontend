import styled from "styled-components";

export const WrapperRadioInputStyled = styled.div`
  
`

export const RadioInputStyled = styled.input`
  float: left;
  display: none;
  margin-left: 15px;
  margin-top: 5px;
`

interface IWrapperRadioItemStyled {
  checked: boolean;
}

export const WrapperRadioItemStyled = styled.div<IWrapperRadioItemStyled>`
  width: 400px;
  max-width: 100%;
  clear: both;
  margin-top: 4px;
  margin-bottom: 4px;
  transition-duration: 0.2s;
  cursor: pointer;
  input:checked + .radio{
    & > *{
      transform: scale(1);
    }
  }
  ${props => props.checked ? 'opacity: 1;' : 'opacity: 0.4;' }
`

export const RadioTextStyled = styled.div`
  display: inline-block;
  margin-left: 10px;
  font-size: 16px;
`

export const CustomRadioStyled = styled.div`
  width: 14px;
  height: 14px;
  float: left;
  border: 1px solid grey;
  margin-left: 15px;
  border-radius: 14px;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const CustomCheckStyled = styled.div`
  height: 8px;
  width: 8px;
  border-radius: 6px;
  background: grey;
  transition-duration: 0.2s;
  transform: scale(0);
`